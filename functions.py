import subprocess
import os
from shutil import which

def get_ffprobe_command() -> str:
    """ My laptop uses ffprobe, workstation is ffmpeg.ffprobe """

    if which("ffprobe"):
        return "ffprobe"

    else:
        return "ffmpeg.ffprobe"

def get_video_length(filepath: str) -> float:
    """Call ffprobe to get the length of a video file"""
    command = [get_ffprobe_command(),
               "-v",
               "error",
               "-show_entries",
               "format=duration",
               "-of",
               "default=noprint_wrappers=1:nokey=1",
               filepath]

    ret = subprocess.run(command, stdout=subprocess.PIPE)
    video_length = float(ret.stdout)
    return video_length

def stream_video(filepath: str):
    """ Use ffmpeg to stream a video as a webcam source """
    command = ['ffmpeg',
               '-re',
               '-i',
               filepath,
               '-vf',
               'format=yuv420p',
               '-f',
               'v4l2',
               '/dev/video2']

    subprocess.Popen(command) # Run in background so main script can continue

def get_video_codec(filepath: str)-> str:
    """ Use ffprobe to get the video encoding of a file """

    command = [get_ffprobe_command(),
               "-v",
               "error",
               '-select_streams',
               'v:0',
               "-show_entries",
               "stream=codec_name",
               "-of",
               "default=noprint_wrappers=1:nokey=1",
               filepath]


    ret = subprocess.run(command, stdout=subprocess.PIPE, text=True)
    codec = str(ret.stdout).strip()
    return codec

def get_video_width(filepath: str)-> str:
    """ Use ffprobe to get the video width """

    command = [get_ffprobe_command(),
               "-v",
               "error",
               '-select_streams',
               'v:0',
               "-show_entries",
               "stream=width",
               "-of",
               "default=noprint_wrappers=1:nokey=1",
               filepath]


    ret = subprocess.run(command, stdout=subprocess.PIPE, text=True)
    width = int(str(ret.stdout).strip())
    return width

def generate_blank_frames(codec: str, width: int, out_file: str = 'blank_frames.mp4', length: int = 5):
    """ Generate mp4 file with blank frames """
    command = ['ffmpeg',
               '-y',
               '-t',
               str(length),
               '-f',
               'lavfi',
               '-i',
               f'color=c=black:s={width}x1080',
               '-c:v',
               codec,
               '-tune',
               'stillimage',
               '-pix_fmt',
               'yuv420p',
               out_file]

    subprocess.run(command, stdout=subprocess.PIPE)

    return out_file

def create_concat_file(*argv: str):
    """ Create ffmpeg concat file concat.txt """

    concat_file = 'concat.txt'
    mode = 'w' if os.path.exists(concat_file) else 'x'
    with open(concat_file, mode) as f:
        for x in argv:
            f.write(f'file {x}\n')

    return concat_file

def concat_videos(concat_file: str = 'concat.txt', output_file: str = 'concat_out.mp4'):
    """ Call ffmpeg command to concatenate videos """
    command = ['ffmpeg',
               '-y',
               '-f',
               'concat',
               '-safe',
               '0',
               '-i',
               concat_file,
               '-c',
               'copy',
               output_file]

    ret = subprocess.run(command, stdout=subprocess.PIPE)
    return output_file

