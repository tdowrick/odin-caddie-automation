# Setup

### Install video4linux

I had to disable secure boot in BIOS settings

    sudo apt-get install -y v4l2loopback-dkms

### Setup v4l for fake webcam

Needs to be repeated if machine restarted

    sudo rmmod v4l2loopback
    sudo modprobe v4l2loopback devices=1 video_nr=2 exclusive_caps=1 card_label="Virtual Webcam"

video_nr=2 sets the webcam to appear on /dev/video2

### Disable local webcam (if there is one)

    sudo modprobe -r uvcvideo # Disabled until reboot

    sudo modprobe uvcvideo # Enable again

### Setup credentials

    export CADDIE_PASSWORD=

### Setup Webdriver

Select appropriate version for Chrome version being used (https://chromedriver.chromium.org/downloads)

Add chromedriver executable to path. e.g. for Tom's laptop it was:

    export PATH=$PATH:/home/tom/Code/Odin/caddie_regulation/chromedriver_linux64

### Install FFMPEG if needed

### Setup Python Environment

    pip install -r requirements.txt

# Running automation

Single

    python run_processing.py endoscopic_video.mp4

Process all files in a folder

    ./run_batch.sh video_dir

The concatenated videos are saved in the `output` folder.

# Misc notes

### Get video length

ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 input.mp4

### Pad video

ffmpeg -i input.mp4 -vf "pad=1920:height=1080:x=336:y=0:color=black" /home/tom/Videos/odin_pad.mp4
x/y are the distance from the left/top

https://superuser.com/questions/690021/video-padding-using-ffmpeg/690211

### Gen black frames

ffmpeg -t 7200 -f lavfi -i color=c=black:s=640x480 -c:v libx264 -tune stillimage -pix_fmt yuv420p output.mp4

### Concat frames

https://stackoverflow.com/questions/7333232/how-to-concatenate-two-mp4-files-using-ffmpeg

ffmpeg -f concat -safe 0 -i concat.txt -c copy concat.mp4
