import time
import os
import sys
import logging
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from functions import get_video_length, stream_video, concat_videos, \
    create_concat_file, get_video_codec, generate_blank_frames, \
        get_video_width

logging.basicConfig(filename="log.txt",
                            filemode='a',
                            format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                            datefmt='%H:%M:%S',
                            level=logging.DEBUG)

# Select username for CADDIE depending on video resolution
USERNAMES_BY_RESOLUTION = {1920: "tdowrick",
                           1248: "tdowrick_1248",
                           1250: "tdowrick_1250"}


CADDIE_PASSWORD = os.environ.get("CADDIE_PASSWORD")

# Create folders for output
video_out_dir = "output"
os.makedirs(video_out_dir, exist_ok=True)

video_file = sys.argv[1]

# Create output files with correct extension
_, extension = os.path.splitext(video_file)
filename_no_path = os.path.basename(video_file)
blank_frames_file = f'blank_frames{extension}'
concat_file = os.path.join(video_out_dir, filename_no_path)

# Create concatenated video
logging.info("Concatenating blank frames to video")
codec = get_video_codec(video_file)
width = get_video_width(video_file)
logging.info(f'Video codec is {codec}')
logging.info(f'Video width is {width}')

CADDIE_USERNAME = USERNAMES_BY_RESOLUTION[width]

blank_frames = generate_blank_frames(codec, width, blank_frames_file)

create_concat_file(blank_frames, video_file)
concatenated_video = concat_videos(output_file=concat_file)

video_length = get_video_length(concatenated_video)

logging.info(f"Concatenated Video: {video_file} has length {video_length}")

# Don't ask for webcam permssions
chrome_options = Options()
chrome_options.add_argument("--use-fake-ui-for-media-stream")
#chrome_options.add_argument("--headless")

# enable browser logging of all events
d = DesiredCapabilities.CHROME
d['goog:loggingPrefs'] = { 'browser':'ALL' }


driver = webdriver.Chrome(options=chrome_options, desired_capabilities=d)

driver.get('https://caddie.odin-vision.com/');

username_box = driver.find_element(By.ID, 'id_username')
password_box = driver.find_element(By.ID, 'id_password')

username_box.send_keys(CADDIE_USERNAME)
password_box.send_keys(CADDIE_PASSWORD)

password_box.submit()

#press space to go to next screen
webdriver.ActionChains(driver).send_keys(Keys.SPACE).perform()

# Start video stream, short pause to allow stream to start,
# then switch camera source in CADDIE
# This needs to be started before CADDIE recording starts
stream_video(concatenated_video)
time.sleep(1)
webdriver.ActionChains(driver).send_keys("S").perform()

# # Start recording
webdriver.ActionChains(driver).send_keys("R").perform()

patient_id_box = driver.find_element(By.ID, "patient_id")
start_button = driver.find_element(By.ID, "id-validation")

patient_id_box.send_keys(filename_no_path)
start_button.click()

time.sleep(video_length)

webdriver.ActionChains(driver).send_keys("R").perform()

logging.info(f"Video {video_file} finished.")

end_procedure_button = driver.find_element(By.ID, "leave")
end_procedure_button.click()

time.sleep(180)

# Get the console log
brower_console_log = driver.get_log('browser')
found_complete_message = "Failed"
# Log last 10 entries
for entry in brower_console_log:
    logging.info(entry)
    if "Upload completed." in str(entry):
        found_complete_message = "Completed"

logging.info(f"Video completion status for {video_file} is {found_complete_message}")

driver.quit()