import time
import os
import sys
import logging

from functions import get_video_length, stream_video, concat_videos, \
    create_concat_file, get_video_codec, generate_blank_frames, \
        get_video_width

logging.basicConfig(filename="log.txt",
                            filemode='a',
                            format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                            datefmt='%H:%M:%S',
                            level=logging.DEBUG)
video_file = sys.argv[1]


# Create folders for output
video_out_dir = "output"
os.makedirs(video_out_dir, exist_ok=True)

# Create output files with correct extension
_, extension = os.path.splitext(video_file)
filename_no_path = os.path.basename(video_file)

blank_frames_file = f'blank_frames{extension}'
concat_file = os.path.join(video_out_dir, filename_no_path)

# Create concatenated video
logging.info("Concatenating blank frames to video")
codec = get_video_codec(video_file)
width = get_video_width(video_file)
logging.info(f'Video codec is {codec}')
logging.info(f'Video width is {width}')

blank_frames = generate_blank_frames(codec, width, blank_frames_file)

create_concat_file(blank_frames, video_file)
concatenated_video = concat_videos(output_file=concat_file)

video_length = get_video_length(concatenated_video)

logging.info(f"Concatenated Video: {video_file} has length {video_length}")